
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

typedef struct users_t
    {
        short id;
        char user_name[20];
        char surname[20];
        short age;
    } user_t;

int main()
{
    user_t a;

    int fd[2];
    if(pipe(fd) == -1) 
    {
        printf("error with pipe(), pls check descriptors array and system file capacity ! ");
        exit(1);
    }

    int fork_ID = fork();
    if(fork_ID == -1)
    {
        printf("!-----couldn't create child process");
        exit(1);
    }

    if(fork_ID == 0)  //child process
    {
        close(fd[0]);

        printf("enter id: ");
        scanf("%hd", &a.id);
        printf("enter username: ");
        scanf("%s", &a.user_name);
        printf("enter surname: ");
        scanf("%s", &a.surname);
        printf("enter age: ");
        scanf("%hd", &a.age);

        write(fd[1], &a, 45);   //writing values of a in fd[1]
        close(fd[1]);
    }
    else   //parent process
    {
        close(fd[1]);
        user_t myUser;
        read(fd[0], &myUser, 45);   //reading from fd[0] to myUser

        printf("-----------------------------");
        printf("\n%hd. %s %s - %hd years old\n", myUser.id, myUser.user_name, myUser.surname, myUser.age);
        close(fd[0]);

        exit(0);
    }

    return 0;
}